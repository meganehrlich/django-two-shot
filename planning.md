Set up
* [x] Fork and clone the starter project from Django * [] Two-Shot 
* [x] Create a new virtual environment
* [x] Activate the virtual environment
* [x] Upgrade pip
* [x] Install django
* [x] Install black
* [x] Install flake8
* [x] Install djhtml
* [x] Install djlint
* [x] Use pip freeze to generate a requirements.txt file
* [x] Create a Django project named "expenses" 
* [x] Run migrations
* [x] Create a superuser 

Feature 2
* [x] Create a Django app named accounts and install it 
* [x] Create a Django app named receipts and install it
* [x] Run the migrations
* [x] Create a super user *repeat step?*
* [x] Testing

Feature 3

The ExpenseCategory model should have:
* [x] a name property 
* [x] an owner property that is a foreign key to the User  model with:
    * [x] a related name of "categories"
    * [x] a cascade deletion relation
    * [x] a __str__ method that returns the name value
The Account model:
* [x] a name property
* [x] a number property
* [x] an owner property that is a foreign key to the User model with:
    * [x] a related name of "accounts"
    * [x] a cascade deletion relation
    * [x] a __str__ method that returns the name value
The Receipt model should have:
* [x] a vendor property
* [x] a total property that is a DecimalField with:
    * [x] three decimal places
    * [x] a maximum of 10 digits
* [x] a tax property that is a DecimalField with:
    * [x] three decimal places
    * [x] a maximum of 10 digits
* [x] a date property 
* [x] a purchaser property that is a foreign key to the User model with:
    * [x] a related name of "receipts"
    * [x] a cascade deletion relation
* [x] a category property that is a foreign key to the ExpenseCategory model with:
    * [x] a related name of "receipts"
    * [x] a cascade deletion relation
* [x] an account property that is a foreign key to the Account model with:
    * [x] a related name of "receipts"
    * [x] a cascade deletion relation
    * [x] allowed to be null
* [x] Testing

Feature 4
* [x] Register models to admin
* [x] test

Feature 5
* [x] Create a view that will get instances of Receipt model and put them in  context for the template.
* [x] Register in the receipts app for path "" and the name "home" in receipts/urls.py.
* [x] Include the URL patterns from receipts app in expenses project with the prefix "receipts/".
* [x] Create a template for the list view that complies with the following specifications.
Template specifications
* [x] the fundamental five in it
* [x] a main tag that contains:
    * [x] an h1 tag with the content "My Receipts"
    * [x] a table that has 6 columns:
        * [x] the first with the header "Vendor" and the rows with the vendor name of the receipt
        * [x] the second with the header "Total" and the total value from the receipt because we don't yet have tasks
        * [x] The third with the header "Tax" and the tax value from the receipt
        * [x] The fourth with the header "Date" and the date value formatted as "m/d/YY"
        * [x] the fifth with the header "Category" and the name of the category for the receipt
        * [x] The sixth with the header "Account" and the name of the account for the receipt
* [x] Testing

Feature 6
* [x] Use RedirectView to redirect from "" to "home" in project urls
* [x] Test

Feature 7
* [x] Register the LoginView  in your accounts urls.py with the path "login/" and the name "login".
* [x] Include the URL patterns from the accounts app in the expenses project with the prefix "accounts/".
* [x] Create a templates directory under accounts.
* [x] Create a registration directory under templates.
* [x] Create an HTML template named login.html in the registration directory.
* [x] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below).
* [x] In the expenses settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home", which will redirect us to the path (not yet created) with the name "home".
Template specifications
* [x] the fundamental five in it
* [x] a main tag that contains:
    * [x] a div tag that contains:
      * [x] an h1 tag with the content "Login"
      * [x] a form tag with method "post" that contains any kind of HTML structures but must include:
        * [x] an input tag with type "text" and name "username"
        * [x] an input tag with type "password" and name "password"
        * [x] a button with the content "Login"
* [x] Testing

Feature 8
* [x] Protect the list view for the Receipt model so that only a person who has logged in can access it.
* [x] Change the queryset of the view to filter the Receipt objects where purchaser equals the logged in user.
* [x] Test

Feature 9
* [x] Import the LogoutView from the same module that you imported the LoginView from.
* [x] Register that view in your urlpatterns list with the path "logout/" and the name "logout".
* [x] In the expenses settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login", which will redirect the logout view to the login page.

Feature 10
* [x] import the UserCreationForm from the built-in auth forms  .
* [x] use special create_user method to create new user account from their username and password.
* [x] use the login  function that logs an account in.
* [x] After you've created user, redirect to the path "home".
* [x] Create an HTML template named signup.html in the registration directory.
* [x] Put a post form in the signup.html and other required HTML or template inheritance stuff
    Template specifications
    * [x] the fundamental five in it
    * [x] a main tag that contains:
        * [x] a div tag that contains:
            * [x] an h1 tag with the content "Signup"
            * [x] a form tag with method "post" that contains any kind of HTML structures but must include:
                * [x] an input tag with type "text" and name "username"
                * [x] an input tag with type "password" and name "password1"
                * [x] an input tag with type "password" and name "password2"
                * [x] a button with the content "Signup"
                * [x] Add links
                 
  * [x] On each of the pages (if you used template inheritance, you should only have to do this in your base.html), add links for the following:
    If a user is signed in:
        * [x] A link to the url named "logout" with the content "Logout"
    If a user is not signed in:
        * [x] A link to the url named "signup" with the content "Sign Up"
        * [x] A link to the url named "login" with the content "Login"
Use the following structure for your navigation:

    * [x] nav element
        * [x] ul element
            * [x] li element for each link
                * [x] a element inside each li with an href for the correct URL
* [x] Testing

* [x] Feature 11
Create a create view for the Receipt model that will show the vendor, total, tax, date, category, and account properties in the form and handle the form submission to create a new Receipt.
* [x] A person must be logged in to see the view.
* [x] Register that view for the path "create/" in the receipts urls.py and the name "create_receipt".
* [x] Create an HTML template that shows the form to create a new Receipt (see the template specifications below).
* [x] Test

Feature 12
* [x] Create expense and account list views
* [x] test

Feature 13
* [x] Create a create view for the ExpenseCategory model that will show the name property in the form and handle the form submission to create a new ExpenseCategory.
* [x] A person must be logged in to see the view.
* [x] Register that view for the path "categories/create/" in the receipts urls.py and the name "create_category".
* [x] Create an HTML template that shows the form to create a new ExpenseCategory (see the template specifications below).
* [x] Test


Feature 14
* [x] Create account and View accounts
* [x] Test

Feature 15
* [x] Already added nav links!