from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, ExpenseCatForm, AccountForm


# Create your views here.

@login_required
def list_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)

        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')

    else:
        form = ReceiptForm()
    context = {
        "form": form
    }
    return render(request, 'receipts/create.html', context)


@login_required
def list_categories(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expenses": expenses
    }
    return render(request, 'expense/categories.html', context)


@login_required
def list_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts
    }
    return render(request, 'accounts/list.html', context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCatForm(request.POST)

        if form.is_valid():
            expense = form.save(commit=False)
            expense.owner = request.user
            expense.save()
            return redirect('list_categories')
    else:
        form = ExpenseCatForm()
    context = {
        'form': form
    }
    return render(request, 'expense/create.html', context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)

        if form.is_valid():
            newaccount = form.save(commit=False)
            newaccount.owner = request.user
            newaccount.save()
            return redirect('list_accounts')

    else:
        form = AccountForm()
    context = {
        'form': form
    }
    return render(request, 'accounts/create.html', context)
