from django.urls import path
from receipts.views import (list_receipts,
                            create_receipt,
                            list_accounts,
                            list_categories,
                            create_category,
                            create_account
                            )

urlpatterns = [
    path('', list_receipts, name="home"),
    path('create/', create_receipt, name='create_receipt'),
    path('accounts/', list_accounts, name="list_accounts"),
    path('categories/', list_categories, name="list_categories"),
    path('categories/create/', create_category, name="create_category"),
    path('accounts/create/', create_account, name='create_account')
]
