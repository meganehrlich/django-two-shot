from django.contrib import admin
from .models import Receipt, Account, ExpenseCategory

# Register your models here.
admin.site.register(Account)


class ExpenseCatAdmin(admin.ModelAdmin):
    pass


admin.site.register(ExpenseCategory)


class ReceiptAdmin(admin.ModelAdmin):
    pass


admin.site.register(Receipt, ReceiptAdmin)
